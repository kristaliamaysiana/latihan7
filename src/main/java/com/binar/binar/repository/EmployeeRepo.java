package com.binar.binar.repository;

import com.binar.binar.entity.Employee;
import com.binar.binar.model.EmployeeModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {

    // JPQL ==============================
    @Query("select e from Employee e WHERE e.id = :id")
    public Employee getbyID(@Param("id") Long id);

    @Query("SELECT e FROM Employee e")// nama class
    public List<Employee> getList();

    @Query("SELECT e FROM Employee e WHERE e.name = :name")// nama class
    Page<Employee> getbyName(String name, Pageable pageable);

    // JPA ===================
    Page<Employee> findByName(String name, Pageable pageable);

    Page<Employee> findByNameLike(String name, Pageable pageable);

    // step 2 DTO ENTITAS :
    @Query(value="select new com.binar.binar.model.EmployeeModel(e.id, e.name) from Employee e")
    List<EmployeeModel> modelDTO();

    // Native ==========================
    @Query(value="select * from Employee e where e.name= :name", nativeQuery=true)
    Object[] getbyNameNative(@Param("name") String name);

    @Query(value="select count(*) from Employee e", nativeQuery=true)
    Long countData();

    @Modifying
    @Query(value="delete from Employee e where e.id= :id", nativeQuery = true)
    void deletenativebyid(@Param("id") Long id);

    @Modifying
    @Query(value="update Employee set name= :name where id = :id", nativeQuery=true)
    void updatenativebyid(@Param("id") Long id, @Param("name") String name);

    // step 2
    @Query(value = "SELECT e.id, e.nama FROM Employee e ", nativeQuery = true)
    List<Object[]> getDataAllNative();

}

