package com.binar.binar.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class EmployeeModel {
    private Long id;
    private String name;
    private String gender;
    private LocalDate dob;
    private String address;
    private int status;
    public  EmployeeModel(Long id, String name){
        System.out.println("id ="+id + " name="+name);
        this.id = id;
        this.name= name;
    }
}
